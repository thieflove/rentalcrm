﻿namespace RentalCRM.ViewModel
{
    public class ReportByCustomerViewModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int TotalMoney { get; set; }
        public string Phone { get; set; }
    }
}
