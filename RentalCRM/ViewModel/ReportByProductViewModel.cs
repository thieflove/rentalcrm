﻿namespace RentalCRM.ViewModel
{
    public class ReportByProductViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int TotalMoney { get; set; }
    }
}
