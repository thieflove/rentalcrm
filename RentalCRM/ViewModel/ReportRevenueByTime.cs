﻿

using System;

namespace RentalCRM.ViewModel
{
    public class ReportRevenueByTimeViewModel
    {
        public string CategoryName { get; set; }
        public DateTime Date { get; set; }
        public int TotalMoney { get; set; }

    }

}
