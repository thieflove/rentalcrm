﻿namespace RentalCRM.ViewModel
{
    public class ReportByProductCategoryViewModel
    {
        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public int TotalMoney { get; set; }
    }
}
